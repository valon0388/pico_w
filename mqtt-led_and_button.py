# Author: Blaine Scroggs
# This module creates an mqtt based device for use with Home Assistant. It
# controls an led's ON/OFF state as well as handling registration and status
# updates in Home Assistant. This is a proof of concept for other work that
# I'll be doing in the future for my smart home setup.


import time
import network
import machine
import json
import ubinascii
from umqttsimple import MQTTClient
from conf import ssid, password, mqtt_server, port, base_name, led_pin
from conf import led_name, button_pin

# Setup
# --------------------------------------
print(f"Wifi Connection Details: ssid({ssid}) password({password})")
print(f"MQTT Connection Details: {mqtt_server}:{port}")
print(f"base_name: {base_name}")

client = ""
client_id = ubinascii.hexlify(machine.unique_id()).decode("utf-8")
print("client_id: {}".format(client_id))
discovery_prefix = "homeassistant"
component = "switch"
object_id = "{}-{}".format(base_name, client_id)

# LED Config
# --------------------------------------
led = machine.Pin(led_pin, machine.Pin.OUT)
led_state = "OFF"
led_config_topic = b"{}/{}/{}/config".format(discovery_prefix, component, object_id)
led_state_topic = b"{}/{}/{}/state".format(discovery_prefix, component, object_id)
led_command_topic = b"{}/{}/{}/set".format(discovery_prefix, component, object_id)
led_payload_on = b"ON"
led_payload_off = b"OFF"
led_state_on = b"ON"
led_state_off = b"OFF"
led_config_payload = {
    "name": led_name,
    "command_topic": led_command_topic,
    "state_topic": led_state_topic,
    "payload_on": led_payload_on,
    "payload_off": led_payload_off,
    "state_on": led_state_on,
    "state_off": led_state_off,
    "qos": 0,
    "retain": "true",
}

# Button Config
# -------------------------------------
button = machine.Pin(button_pin, machine.Pin.IN, machine.Pin.PULL_DOWN)
button_oldstate = ""

last_message = 0
message_interval = 5
counter = 0


# Connect Wifi
# --------------------------------------
def setupWifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(ssid, password)

    led.off()

    # Wait for connect or fail
    max_wait = 10
    while max_wait > 0:
        led.on()
        if wlan.status() < 0 or wlan.status() >= 3:
            break
        time.sleep(0.5)
        led.off()
        max_wait -= 1
        print("waiting for connection...")
        time.sleep(0.5)

    # Handle connection error
    if wlan.status() != 3:
        raise RuntimeError("network connection failed")
    else:
        print("connected")
        status = wlan.ifconfig()
        print("connection = {}".format(status))


def led_ON():
    global led_state
    print("Set LED state to ON!")
    client.publish(led_state_topic, led_state_on)
    led_state = "ON"
    led.on()


def led_OFF():
    global led_state
    print("Set LED state to OFF!")
    client.publish(led_state_topic, led_state_off)
    led_state = "OFF"
    led.off()


def sub_cb(topic, msg):
    if topic == led_command_topic and msg == led_payload_on:
        print("Callback for {} with payload {}, turning led ON".format(topic, msg))
        led_ON()
    elif topic == led_command_topic and msg == led_payload_off:
        print("Callback for {} with payload {}, turning led OFF".format(topic, msg))
        led_OFF()


def connect_and_subscribe():
    global client, client_id, mqtt_server, port, topic_sub
    client = MQTTClient(client_id, mqtt_server, port=port)
    client.set_callback(sub_cb)
    client.connect()
    client.subscribe(led_command_topic)
    print(
        "Connected to {}:{} MQTT Broker, subscribed to {} topic".format(
            mqtt_server, port, led_command_topic
        )
    )
    client.publish(led_config_topic, json.dumps(led_config_payload))
    print(
        "     Published {} with payload: {}".format(
            led_config_topic, led_config_payload
        )
    )
    return client


def restart_and_reconnect():
    print("Failed to connect to MQTT Broker. Reconnecting...")
    time.sleep(1)
    machine.reset()


setupWifi()
time.sleep(3)
led.off()

try:
    client = connect_and_subscribe()
except OSError as e:
    restart_and_reconnect()

while True:
    try:
        client.check_msg()
        # print("Button value: {}".format(button.value()))
        if button.value() is 1 and button_oldstate is 0:
            button_oldstate = 1
            print("Detected a button press!!!")
            if led_state is "OFF":
                led_ON()
            elif led_state is "ON":
                led_OFF()
        elif button.value() is 0:
            button_oldstate = 0
        if (time.time() - last_message) > message_interval:
            last_message = time.time()
            counter += 1
    except OSError as e:
        restart_and_reconnect()
    # time.sleep(0.1)
