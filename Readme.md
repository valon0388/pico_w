# Pico W Projects

The following contains a demo of Raspberry Pi Pico W code, written in MicroPython, as well as some rshell macros, written in bash, that can be sourced to help with loading the code to a device. Listed below, are the sources referenced to create the code.

### ======================== Reference URLS ========================

##### ------------ Pico W ------------

[Firmware and Official Docs](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html)

[Pi Pico SDK](https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-python-sdk.pdf)

[Connecting to the Internet](https://datasheets.raspberrypi.com/picow/connecting-to-the-internet-with-pico-w.pdf)

[Datasheet](https://datasheets.raspberrypi.com/picow/pico-w-datasheet.pdf)

[Getting Started](https://www.cnx-software.com/2022/07/03/getting-started-with-wifi-on-raspberry-pi-pico-w-board/)

[Setup](https://www.tomshardware.com/how-to/raspberry-pi-pico-setup)

[Setup MicroPython](https://makersportal.com/blog/raspberry-pi-pico-tests-with-micropython)

[Fuzix](https://www.raspberrypi.com/news/how-to-get-started-with-fuzix-on-raspberry-pi-pico/)



